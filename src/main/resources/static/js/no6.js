$('[data-toggle="tooltip"]').tooltip()

var row = null
function clickSubmit() {
  if (row === null) {
    insertData()
  } else {
    editRow()
  }
  resetForm()
}

function insertData() {
  document.getElementById("empty-table").insertRow(-1).innerHTML = '<tr id="row"><td>' + $('#exampleInputName').val() +
    '</td><td>' + $('#exampleInputDate').val() +
    '</td><td>' + $('.input-gender:checked').val() +
    '</td><td>' + $('#exampleInputAddress').val() +
    '</td><td>' + $('#agama').val() +
    '</td><td>' + $('#exampleInputNumber').val()+
    '</td><td>' + $('#exampleInputEmail1').val() +
    '</td><td>' + $('#exampleInputFathersName').val() +
    '</td><td>' + $('#exampleInputFathersJob').val() +
    '</td><td>' + $('#exampleInputMothersName').val() +
    '</td><td>' + $('#exampleInputMothersJob').val() +
    '</td><td>' + $('#exampleInputAddress2').val() +
    '</td><td>' + $('#agama2').val() +
    '</td><td>' + $('#exampleInputFNumber').val() +
    '</td><td>' + $('#exampleInputEmail2').val() +
    '</td><td>' + '<div id="tombol"><a id="edt" class="btn edit" title="edit" data-toggle="tooltip" onclick="upValueToForm(this)"><i class="fa fa-pencil-alt"></i></a><a id="dlt" class="btn delete" title="delete" data-toggle="tooltip"><i class="fa fa-trash"></i></a></div>' +
    '</td></tr>';
}

function resetForm() {
  $('input[type="text"]').val("")
  $('input[type="email"]').val("")
  $('input[type="number"]').val("")
  $('input[type="date"]').val("")
  $('input[name="customRadio"]').prop('checked', false)
  $('#agama').val("")
  $('#agama2').val("")
  $('#exampleInputAddress').val("")
  $('#exampleInputAddress2').val("")
  row = null
}

function upValueToForm(a) {
  row = a.parentElement.parentElement.parentElement
  document.getElementById("exampleInputName").value = row.cells[0].innerHTML
  document.getElementById("exampleInputDate").value = row.cells[1].innerHTML
  $('input[name="customRadio"]').prop('checked', true)
  document.getElementById("exampleInputAddress").value = row.cells[3].innerHTML
  document.getElementById("agama").value = row.cells[4].innerHTML
  document.getElementById("exampleInputNumber").value = row.cells[5].innerHTML
  document.getElementById("exampleInputEmail1").value = row.cells[6].innerHTML
  document.getElementById("exampleInputFathersName").value = row.cells[7].innerHTML
  document.getElementById("exampleInputFathersJob").value = row.cells[8].innerHTML
  document.getElementById("exampleInputMothersName").value = row.cells[9].innerHTML
  document.getElementById("exampleInputMothersJob").value = row.cells[10].innerHTML
  document.getElementById("exampleInputAddress2").value = row.cells[11].innerHTML
  document.getElementById("agama2").value = row.cells[12].innerHTML
  document.getElementById("exampleInputFNumber").value = row.cells[13].innerHTML
  document.getElementById("exampleInputEmail2").value = row.cells[14].innerHTML
}

// $(document).on('click', '.edit', function (){
//   row = $(this).closest("tr");
//   document.getElementById("exampleInputName").value = row.find("td").eq(0).text()
//   document.getElementById("exampleInputDate").value = row.find("td").eq(1).text()
//   $('input[name="customRadio"]').prop('checked', true)
//   document.getElementById("exampleInputAddress").value = row.find("td").eq(3).text()
//   document.getElementById("agama").value = row.find("td").eq(4).text()
//   document.getElementById("exampleInputNumber").value = row.find("td").eq(5).text()
//   document.getElementById("exampleInputEmail1").value = row.find("td").eq(6).text()
//   document.getElementById("exampleInputFathersName").value = row.find("td").eq(7).text()
//   document.getElementById("exampleInputFathersJob").value = row.find("td").eq(8).text()
//   document.getElementById("exampleInputMothersName").value = row.find("td").eq(9).text()
//   document.getElementById("exampleInputMothersJob").value = row.find("td").eq(10).text()
//   document.getElementById("exampleInputAddress2").value = row.find("td").eq(11).text()
//   document.getElementById("agama2").value = row.find("td").eq(12).text()
//   document.getElementById("exampleInputFNumber").value = row.find("td").eq(13).text()
//   document.getElementById("exampleInputEmail2").value = row.find("td").eq(14).text()
// })

function editRow() {
  row.cells[0].innerHTML = $('#exampleInputName').val()
  row.cells[1].innerHTML = $('#exampleInputDate').val()
  row.cells[2].innerHTML = $('.input-gender:checked').val()
  row.cells[3].innerHTML = $('#exampleInputAddress').val()
  row.cells[4].innerHTML = $('#agama').val()
  row.cells[5].innerHTML = $('#exampleInputNumber').val()
  row.cells[6].innerHTML = $('#exampleInputEmail1').val()
  row.cells[7].innerHTML = $('#exampleInputFathersName').val()
  row.cells[8].innerHTML = $('#exampleInputFathersJob').val()
  row.cells[9].innerHTML = $('#exampleInputMothersName').val()
  row.cells[10].innerHTML = $('#exampleInputMothersJob').val()
  row.cells[11].innerHTML = $('#exampleInputAddress2').val()
  row.cells[12].innerHTML = $('#agama2').val()
  row.cells[13].innerHTML = $('#exampleInputFNumber').val()
  row.cells[14].innerHTML = $('#exampleInputEmail2').val()
}
